using Foundation;
using Supermodel.Mobile.Runtime.iOS.App;
using SIS.Mobile.AppCore;

namespace SIS.Mobile.iOS
{
    [Register("AppDelegate")]
    public class AppDelegate : iOSFormsApplication<SISApp> { }
}