#nullable enable

using Supermodel.Mobile.Runtime.Common.DataContext.CachedWebApi;

namespace SIS.Mobile.Supermodel.Persistence
{
    public class SISCachedWebApiDataContext : CachedWebApiDataContext<SISWebApiDataContext, SISSqliteDataContext> {}
}
