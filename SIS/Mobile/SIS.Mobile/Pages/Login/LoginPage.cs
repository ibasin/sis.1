#nullable enable

using System.Threading.Tasks;
using Supermodel.ApiClient.Models;
using Supermodel.Mobile.Runtime.Common.XForms.Pages.Login;
using Xamarin.Forms;
using SIS.Mobile.AppCore;
using SIS.Mobile.Pages.Home;
using SIS.Mobile.Supermodel.Persistence;

namespace SIS.Mobile.Pages.Login
{
    public class LoginPage : UsernameAndPasswordLoginPage<SISUserUpdatePassword, SISWebApiDataContext>
    {
        #region Constructors
        public LoginPage()
        {
            var logoImage = new Image
            {
                Aspect = Aspect.AspectFit,
                #pragma warning disable 618
                Source = ImageSource.FromResource("SIS.Mobile.EmbeddedResources.Logo.png"),
                #pragma warning restore 618
                WidthRequest = 300,
                HorizontalOptions = LayoutOptions.CenterAndExpand
            };

            LoginView.ContentView.SetUpLoginImage(logoImage);
        }
        #endregion

        #region Overrdies
        public override async Task<bool> OnSuccessfulLoginAsync(bool autoLogin, bool isJumpBack)
        {
            var homePage = SISApp.RunningApp.HomePage = new HomePage();
            await Navigation.PushAsync(homePage);
            return true;
        }
        public override IAuthHeaderGenerator GetAuthHeaderGenerator(UsernameAndPasswordLoginViewModel loginViewModel)
        {
            return new BasicAuthHeaderGenerator(loginViewModel.Username, loginViewModel.Password, SISApp.RunningApp.LocalStorageEncryptionKey);
        }
        #endregion
    }
}
