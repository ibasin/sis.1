#nullable enable

using System.Threading.Tasks;
using Supermodel.ApiClient.Models;
using Supermodel.Mobile.Runtime.Common.UnitOfWork;
using Supermodel.Mobile.Runtime.Common.XForms.App;
using Supermodel.Mobile.Runtime.Common.XForms.Pages.CRUDDetail;
using Supermodel.Mobile.Runtime.Common.XForms.Pages.Login;
using SIS.Mobile.AppCore;
using SIS.Mobile.Models;
using SIS.Mobile.Supermodel.Persistence;

namespace SIS.Mobile.Pages.ChangePassword
{
    public class ChangePasswordPage : CRUDDetailPage<SISUserUpdatePassword, SISUserUpdatePasswordXFModel, SISWebApiDataContext>
    {
        #region Overrides
        protected override async Task SaveItemInternalAsync(SISUserUpdatePassword model)
        {
            if (!string.IsNullOrEmpty(model.OldPassword) && !string.IsNullOrEmpty(model.NewPassword))
            {
                await using (SISApp.RunningApp.NewUnitOfWork<SISWebApiDataContext>())
                {
                    model.Update();
                    await UnitOfWorkContext.FinalSaveChangesAsync();
                    var basicAuthHeader = (BasicAuthHeaderGenerator)FormsApplication<SISApp>.RunningApp.AuthHeaderGenerator;
                    basicAuthHeader.Password = model.NewPassword;
                    await basicAuthHeader.SaveToAppPropertiesAsync();
                }
            }
        }
        #endregion
    }
}
