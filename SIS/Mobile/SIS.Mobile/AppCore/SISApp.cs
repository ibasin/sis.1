#nullable enable

using Supermodel.Mobile.Runtime.Common.XForms;
using Supermodel.Mobile.Runtime.Common.XForms.App;
using Xamarin.Forms;
using SIS.Mobile.Pages.ChangePassword;
using SIS.Mobile.Pages.Home;
using SIS.Mobile.Pages.Login;

namespace SIS.Mobile.AppCore
{
    public class SISApp : SupermodelXamarinFormsApp
    {
        #region Constructors
        public SISApp()
        {
            XFormsSettings.LabelFontSize = XFormsSettings.ValueFontSize = 18;

            LoginPage = new LoginPage();
            MainPage = new NavigationPage(LoginPage);

            #pragma warning disable 4014
            LoginPage.AutoLoginIfPossibleAsync();
            #pragma warning restore 4014

            //Init these properties here because we are in #nullable enable context
            HomePage = new HomePage();
            ChangePasswordPage = new ChangePasswordPage();
        }
        #endregion

        #region Overrides
        public override void HandleUnauthorized()
        {
            LoginPage = new LoginPage();
            MainPage = new NavigationPage(FormsApplication<SISApp>.RunningApp.LoginPage);
        }

        public override byte[] LocalStorageEncryptionKey { get; } = { 0x95, 0x91, 0xD5, 0x21, 0x7A, 0x0B, 0x47, 0x1A, 0xB4, 0x3E, 0xFD, 0x8A, 0x1E, 0x5C, 0x01, 0x16 };
        #endregion

        #region Methods
        public static SISApp RunningApp => FormsApplication<SISApp>.RunningApp;
        #endregion

        #region Properties
        public LoginPage LoginPage { get; set; }
        public HomePage HomePage { get; set; }
        public ChangePasswordPage ChangePasswordPage { get; set; }
        #endregion
    }
}
