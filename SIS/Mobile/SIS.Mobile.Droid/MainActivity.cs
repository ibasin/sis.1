using Android.App;
using SIS.Mobile.AppCore;
using Supermodel.Mobile.Runtime.Droid.App;

namespace SIS.Mobile.Droid
{
    [Activity(Label = "SIS.Droid", MainLauncher = true)]
    public class MainActivity : DroidFormsApplication<SISApp> { }
}

