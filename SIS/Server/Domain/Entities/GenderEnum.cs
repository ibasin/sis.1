﻿#nullable enable

namespace Domain.Entities
{
    public enum GenderEnum
    {
        Male,
        Female,
        NonBinary
    }
}
