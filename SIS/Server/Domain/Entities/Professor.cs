﻿#nullable enable

using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Domain.Entities
{
    public class Professor : Person
    {
        #region Override
        protected override void DeleteInternal()
        {
            Active = false;
        }
        #endregion
        
        #region Properties
        public bool Active { get; set; } = true;
        
        public virtual List<Class> Classes { get; set; } = new List<Class>();
        #endregion
    }

    public class ProfessorModelConfig : IEntityTypeConfiguration<Professor>
    {
        #region Overrides
        public void Configure(EntityTypeBuilder<Professor> builder)
        {
            builder.HasIndex(x => x.Gender);
            builder.HasIndex(x => new { x.LastName, x.FirstName } );
            builder.HasIndex(x => x.SocialSecurity).IsUnique();

            builder.HasIndex(x => x.Active);
        }
        #endregion
    }
}
