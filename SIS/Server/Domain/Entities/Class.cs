﻿#nullable enable

using System.ComponentModel.DataAnnotations;
using Supermodel.Persistence.Entities;

namespace Domain.Entities
{
    public class Class : Entity
    {
        #region Overrides
        protected override void DeleteInternal()
        {
            foreach (var m2MStudentClass in StudentsAttended)
            {
                m2MStudentClass.Delete();
            }
            base.DeleteInternal();
        }
        #endregion
        
        #region Properties
        [Required] public virtual Professor Professor { get; set; } = default!;
        
        public string Name { get; set; } = "";
        public int Credits { get; set; }

        public virtual M2MList<M2MStudentClass> StudentsAttended { get; set; } = new M2MTo1Lis2<M2MStudentClass>();
        #endregion
    }
}
