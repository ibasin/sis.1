#nullable enable

using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Domain.Supermodel.Persistence;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Supermodel.Persistence.Entities;
using Supermodel.Persistence.Entities.ValueTypes;

namespace Domain.Entities
{
    public class SISUser : UserEntity<SISUser, DataContext>
    {
        #region Constants
        public const string AdminRole = "Admin";
        #endregion

        #region Overrides
        protected override void DeleteInternal()
        {
            DeleteToDoItems();
            base.DeleteInternal();
        }
        #endregion

        #region Methods
        public void DeleteToDoItems()
        {
            foreach (var toDoItem in ToDoItems)
            {
                toDoItem.Delete();
            }
            ToDoItems.Clear();
        }
        #endregion
        
        #region Properties
        [Required] public string FirstName { get; set; } = "";
        [Required] public string LastName { get; set; } = "";

        public virtual USAddress Address { get; set; } = new USAddress();
        
        public bool Admin { get; set; }

        [Required] public virtual SISUserPicture SISUserPicture { get; set; } = new SISUserPicture();

        public virtual List<ToDoItem> ToDoItems { get; set; } = new List<ToDoItem>();
        #endregion
    }
    
    public class SISUserModelConfig : IEntityTypeConfiguration<SISUser>
    {
        #region Overrides
        public void Configure(EntityTypeBuilder<SISUser> builder)
        {
            builder.HasOne(a => a.SISUserPicture).WithOne(b => b.SISUser).HasForeignKey<SISUserPicture>();
        }
        #endregion
    }
}
