﻿#nullable enable

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Supermodel.Persistence;
using Supermodel.Persistence.DataContext;
using Supermodel.Persistence.Entities;

namespace Domain.Entities
{
    public class Major : Entity
    {
        #region Overrides
        protected override void DeleteInternal()
        {
            if (Students.Any()) throw new UnableToDeleteException("The major is being used by some of the students"); 
            //foreach (var student in Students)
            //{
            //    student.Delete();
            //}
            base.DeleteInternal();
        }
        public override void BeforeSave(OperationEnum operation)
        {
            if (operation == OperationEnum.Add) DateCreated = DateModified = DateTime.Today;
            if (operation == OperationEnum.Update) DateModified = DateTime.Today;
            base.BeforeSave(operation);
        }
        #endregion
        
        #region Properties
        [Required] public virtual Department Department { get; set; } = default!;
        
        [Required] public string Name { get; set; } = "";
        [Required] public DateTime? DateCreated { get; set; }
        [Required] public DateTime? DateModified { get; set; }

        public virtual List<Student> Students { get; set; } = new List<Student>();
        #endregion
    }

    public class MajorModelConfig : IEntityTypeConfiguration<Major>
    {
        #region Overrides
        public void Configure(EntityTypeBuilder<Major> builder)
        {
            builder.HasIndex(x => x.Name).IsUnique();
        }
        #endregion
    }
}
