﻿#nullable enable

using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;
using Domain.Supermodel.Persistence;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Supermodel.DataAnnotations.Validations;
using Supermodel.Persistence.Entities;
using Supermodel.Persistence.Entities.ValueTypes;
using Supermodel.Persistence.Repository;
using Supermodel.Persistence.UnitOfWork;

namespace Domain.Entities
{
    public class Student : Person
    {
        #region Validation
        public override async Task<ValidationResultList> ValidateAsync(ValidationContext validationContext)
        {
            var vr = await base.ValidateAsync(validationContext);
            await using (new UnitOfWork<DataContext>(ReadOnly.Yes))
            {
                var repo = LinqRepoFactory.Create<Student>();
                if (await repo.Items.AnyAsync(x => x.SocialSecurity == SocialSecurity && (Id == 0 || x.Id != Id))) vr.AddValidationResult(this, "User with this social security already exists", x => x.SocialSecurity);
            }
            return vr;
        }
        protected override void DeleteInternal()
        {
            foreach (var m2MStudentClass in ClassesTaken)
            {
                m2MStudentClass.Delete();
            }
            base.DeleteInternal();
        }
        #endregion
        
        #region Properties
        public virtual USAddress SchoolAddress { get; set; } = new USAddress();

        [Required] public virtual Major Major { get; set; } = default!;
        public virtual M2MList<M2MStudentClass> ClassesTaken { get; set; } = new M2MTo1Lis2<M2MStudentClass>();
        #endregion
    }

    public class StudentModelConfig : IEntityTypeConfiguration<Student>
    {
        #region Overrides
        public void Configure(EntityTypeBuilder<Student> builder)
        {
            builder.HasIndex(x => x.Gender);
            builder.HasIndex(x => new { x.LastName, x.FirstName } );
            builder.HasIndex(x => x.SocialSecurity).IsUnique();
        }
        #endregion
    }
}
