﻿#nullable enable

using System.ComponentModel.DataAnnotations;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Supermodel.Persistence.Entities;

namespace Domain.Entities
{
    public class ToDoItem : Entity
    {
        #region Properties
        [Required] public long SISUserId {get; set; }
        [Required] public virtual SISUser SISUser { get; set; } = default!;
        
        [Required] public string Name { get; set; } = "";
        public int Priority { get; set; } = 10;
        #endregion
    }

    public class ToDoItemModelConfig : IEntityTypeConfiguration<ToDoItem>
    {
        #region Overrides
        public void Configure(EntityTypeBuilder<ToDoItem> builder)
        {
            builder.HasIndex(x => new { x.SISUserId, x.Priority } );
        }
        #endregion
    }
}
