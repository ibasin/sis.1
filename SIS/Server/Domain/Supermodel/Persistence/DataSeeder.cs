#nullable enable

using System.Threading.Tasks;
using Domain.Entities;

namespace Domain.Supermodel.Persistence
{
    public static class DataSeeder
    {
        #region Methods
        public static Task SeedDataAsync()
        {
            var users = new[] 
            { 
                new SISUser { FirstName = "Sample", LastName = "Account", Username="supermodel@noblis.org", Password="1234", Admin = true, SISUserPicture = new SISUserPicture() },
                new SISUser { FirstName = "Ilya", LastName = "Basin", Username="ilya@noblis.org", Password="1234", Admin = false, SISUserPicture = new SISUserPicture() },
                new SISUser { FirstName = "Shiloh", LastName = "Enriquez", Username="shiloh@noblis.org", Password="1234", Admin = false, SISUserPicture = new SISUserPicture() },
                new SISUser { FirstName = "Surendra", LastName = "Upadhaya", Username="surendra@noblis.org", Password="1234", Admin = false, SISUserPicture = new SISUserPicture() },
                new SISUser { FirstName = "Shabbir", LastName = "Mohammad", Username="shabbir@noblis.org", Password="1234", Admin = false, SISUserPicture = new SISUserPicture() },
                new SISUser { FirstName = "Carrie", LastName = "Song", Username="carrie@noblis.org", Password="1234", Admin = false, SISUserPicture = new SISUserPicture() },
            };
            foreach (var user in users) user.Add();

            users[0].ToDoItems.Add(new ToDoItem { Name = "File annual report"});
            users[0].ToDoItems.Add(new ToDoItem { Name = "Approve time sheets"});
            users[0].ToDoItems.Add(new ToDoItem { Name = "Fire annoying employees"});

            users[1].ToDoItems.Add(new ToDoItem { Name = "Prepare for the presentation"});

            var mathMajor = new Major { Name = "Math" };
            mathMajor.Add();
            var englishMajor = new Major { Name = "English" };
            englishMajor.Add();
            var csMajor = new Major { Name = "Computer Science" };
            csMajor.Add();

            var departments = new []
            {
                new Department { Name = "Language", Majors = { englishMajor } },
                new Department { Name ="STEM", Majors = { mathMajor, csMajor } }
            };
            foreach (var department in departments) department.Add();

            var students = new[]
            {
                new Student { FirstName = "Jerry", LastName = "Seinfeld", Gender = GenderEnum.Male, SocialSecurity = "111-11-1111", Major = mathMajor }, 
                new Student { FirstName = "George", LastName = "Costanza", Gender = GenderEnum.Male, SocialSecurity = "222-22-2222", Major = englishMajor }, 
                new Student { FirstName = "Cosmo", LastName = "Kramer", Gender = GenderEnum.NonBinary, SocialSecurity = "333-33-3333", Major = csMajor }, 
                new Student { FirstName = "Elane", LastName = "Benes", Gender = GenderEnum.Female, SocialSecurity = "444-44-4444", Major = englishMajor },
            };
            foreach (var student in students) student.Add();

            var feynmanProfessor = new Professor { FirstName = "Richard", LastName = "Feynman", Gender = GenderEnum.Male, SocialSecurity = "999-99-9999" };
            feynmanProfessor.Add();
            var vonnegutProfessor = new Professor { FirstName = "Kurt", LastName = "Vonnegut", Gender = GenderEnum.Male, SocialSecurity = "888-88-8888" };
            feynmanProfessor.Add();

            var classes = new[]
            {
                new Class { Name = "English 101", Credits = 3, Professor = vonnegutProfessor},
                new Class { Name = "CS 110", Credits = 4, Professor = feynmanProfessor},
                new Class { Name = "Chemistry 110", Credits = 4, Professor = feynmanProfessor }
            };
            foreach (var @class in classes) @class.Add();

            classes[0].StudentsAttended.Add(new M2MStudentClass { Connection1 = students[0] });
            classes[0].StudentsAttended.Add(new M2MStudentClass { Connection1 = students[1] });
            classes[0].StudentsAttended.Add(new M2MStudentClass { Connection1 = students[2] });
            classes[0].StudentsAttended.Add(new M2MStudentClass { Connection1 = students[3] });

            return Task.CompletedTask;
        }
        #endregion
    }
}
