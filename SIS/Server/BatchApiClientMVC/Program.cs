#nullable enable

using System;
using System.Threading.Tasks;
using BatchApiClientMVC.Supermodel.Auth;
using BatchApiClientMVC.Supermodel.Persistence;
using Supermodel.ApiClient.Models;
using Supermodel.Mobile.Runtime.Common.UnitOfWork;
using Supermodel.Mobile.Runtime.Common.XForms.Pages.Login;

namespace BatchApiClientMVC
{
    class Program
    {
        static async Task Main()
        {
            Console.WriteLine("Press Enter to continue...");
            Console.ReadLine();

            //var authHeaderGenerator = new BasicAuthHeaderGenerator("supermodel@noblis.org", "1234");
            var authHeaderGenerator = new SISSecureAuthHeaderGenerator("supermodel@noblis.org", "1234", new byte[0]);

            //await using (new UnitOfWork<SISWebApiDataContext>())
            //{
            //    UnitOfWorkContext.AuthHeader = authHeaderGenerator.CreateAuthHeader();

            //    var user = new SISUserUpdatePassword
            //    {
            //        Id = 1,
            //        OldPassword = "1234",
            //        NewPassword = "12345"
            //    };
            //    user.Update(); //we do update because we need to "attach" the model, make it managed
            //}
            //Console.WriteLine("User with id=1's password updated to '12345'");

            //await using (new UnitOfWork<SISWebApiDataContext>())
            //{
            //    var repo = RepoFactory.Create<Department>();

            //    var departments = await repo.GetAllAsync();
            //    //var department1 = await repo.GetByIdAsync(1);
            //    departments[0].Name = departments[0].Name + " Updated!";
            //    //departments[1].Delete();

            //    var newDepartment = new Department { Name = "New Department" };
            //    newDepartment.Add();

            //    //UnitOfWorkContext.CommitOnDispose = false;
            //}

            //Console.ReadLine();

            //var delayedAllDepartments = new DelayedModels<Department>();
            //await using (new UnitOfWork<SISWebApiDataContext>())
            //{
            //    var repo = RepoFactory.Create<Department>();

            //    //var newDepartment = await repo.GetByIdAsync(3);
            //    var newDepartment = (await repo.GetWhereAsync(new SimpleSearch { SearchTerm = "New Department" })).SingleOrDefault();
            //    if (newDepartment != null) newDepartment.Delete();

            //    repo.DelayedGetAll(out delayedAllDepartments);
            //}

            await using (new UnitOfWork<SISWebApiDataContext>(ReadOnly.Yes))
            {
                UnitOfWorkContext.AuthHeader = authHeaderGenerator.CreateAuthHeader();
                
                var output = await UnitOfWorkContext<SISWebApiDataContext>.CurrentDataContext.ActivateProfessorsApiAsync(new Input { Suffix = " [REACTIVATED]"});
                Console.WriteLine($"{output.ReactivatedCount} professors reactivated!");

                var result = await UnitOfWorkContext<SISWebApiDataContext>.CurrentDataContext.ValidateLoginAsync<Department>();
            }
        }
    }
}
