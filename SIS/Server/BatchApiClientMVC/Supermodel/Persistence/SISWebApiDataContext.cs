#nullable enable

using System;
using System.Net.Http;
using Supermodel.Mobile.Runtime.Common.DataContext.WebApi;

namespace BatchApiClientMVC.Supermodel.Persistence
{
    public class SISWebApiDataContext: WebApiDataContext
    {
        #region Overrides
        public override string BaseUrl => "http://localhost:41176/"; //this one is for MVC

        // set timeout to 10 min, so we can debug
        protected override HttpClient CreateHttpClient()
        {
            var httpClient = base.CreateHttpClient();
            httpClient.Timeout = new TimeSpan(0, 10, 0);
            return httpClient;
        }
        #endregion
    }
}
