#nullable enable

using System;
using Supermodel.Encryptor;
using Supermodel.Mobile.Runtime.Common.XForms.Pages.Login;

namespace BatchApiClientMVC.Supermodel.Auth
{
    public class SISSecureAuthHeaderGenerator : BasicAuthHeaderGenerator
    {
        #region Constructors
        public SISSecureAuthHeaderGenerator(string username, string password, byte[] localStorageEncryptionKey) : base(username, password, localStorageEncryptionKey){}
        #endregion

        #region Overrdies
        public override AuthHeader CreateAuthHeader()
        {
            var dateTimeSalt = HashAgent.Generate5MinTimeStampSalt(DateTime.UtcNow);
            var secretTokenHashSalt = HashAgent.GenerateGuidSalt();
            var secretTokenHash = HashAgent.HashPasswordSHA256(SecretToken + dateTimeSalt, secretTokenHashSalt);
            var authHeader = HttpAuthAgent.CreateSMCustomEncryptedAuthHeader(Key, Username, Password, secretTokenHash, secretTokenHashSalt);
            authHeader.HeaderName = HeaderName;
            return authHeader;
        }
        #endregion

        #region Shared Constants
        public static readonly byte[] Key = { 0xCF, 0xEC, 0xB4, 0x43, 0x6C, 0x8E, 0x82, 0xA8, 0x5F, 0x22, 0xDB, 0xD1, 0xB9, 0x1C, 0x97, 0x4A };
        public static readonly string HeaderName = "X-SIS-Authorization";
        // ReSharper disable StringLiteralTypo
        public static readonly string SecretToken = "JHFV_jhaegvdkjHGVBKJDHgbejdfh**&@$vJHgvkzsdhfbgkb37r3t84r7glSCGO834FG{YD^%^$";
        // ReSharper restore StringLiteralTypo
        #endregion
    }
}
