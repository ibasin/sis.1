﻿#nullable enable

using System.Linq;
using Domain.Entities;
using Domain.Supermodel.Persistence;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Supermodel.Presentation.Mvc.Controllers.Mvc;
using WebMVC.Models;

namespace WebMVC.Controllers
{
    [Authorize]
    public class ClassController : CRUDController<Class, ClassMvcModel, DataContext> 
    { 
        #region Overrides
        protected override IActionResult StayOnDetailScreen(long id)
        {
            return GoToListScreen();
        }
        protected override IQueryable<Class> GetItems()
        {
            return base.GetItems().Include(x => x.StudentsAttended);
        }
        #endregion
    }
}
