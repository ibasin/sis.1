﻿#nullable enable

using System.Linq;
using System.Threading.Tasks;
using Domain.Entities;
using Domain.Supermodel.Persistence;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Supermodel.Presentation.Mvc.Controllers.Mvc;
using WebMVC.Models;

namespace WebMVC.Controllers
{
    [Authorize]
    public class MajorController 
        : ChildCRUDController<Major, MajorDetailMvcModel, Department, DepartmentController, DataContext> 
    {
        #region Overrides
        protected override Task<IActionResult> AfterCreateAsync(long id, long parentId, Major entityItem, MajorDetailMvcModel mvcModelItem)
        {
            return Task.FromResult(StayOnDetailScreen(id));
        }
        protected override IQueryable<Major> GetItems()
        {
            return base.GetItems().Include(x => x.Students);
        }
        #endregion
    }
}
