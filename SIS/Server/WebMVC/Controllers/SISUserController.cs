﻿#nullable enable

using System.Linq;
using System.Threading.Tasks;
using Domain.Entities;
using Domain.Supermodel.Persistence;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Supermodel.Persistence.Repository;
using Supermodel.Persistence.UnitOfWork;
using Supermodel.Presentation.Mvc.Bootstrap4.D3.Models;
using Supermodel.Presentation.Mvc.Bootstrap4.Models;
using Supermodel.Presentation.Mvc.Controllers.Mvc;
using WebMVC.Models;

namespace WebMVC.Controllers
{
    [Authorize]
    [Authorize(Roles = "Admin")]
    public class SISUserController : EnhancedCRUDController<SISUser, SISUserMvcModel, Bs4.SimpleSearchMvcModel, DataContext> 
    {
        #region ActionMethods
        public override async Task<IActionResult> List(int? smSkip = null, int? smTake = null, string? smSortBy = null)
        {
            await using(new UnitOfWork<DataContext>(ReadOnly.Yes))
            {
                var sisUsers = await RepoFactory.Create<SISUser>().GetAllAsync();

                var barChart = new D3.BarChartMvcModel();
                var donutChart = new D3.DonutChartMvcModel();

                foreach (var sisUser in sisUsers)
                {
                    var barDatum = new D3.BarChartMvcModel.Datum(sisUser.Username, sisUser.ToDoItems.Count);
                    barChart.Data.Add(barDatum);

                    var donutDatum = new D3.DonutChartMvcModel.Datum(sisUser.Id, sisUser.Username, sisUser.ToDoItems.Count);
                    donutChart.Data.Add(donutDatum);
                }
                
                barChart.LabelsNumberFormat = "d";
                ViewBag.BarChart = barChart;
                
                ViewBag.DonutChart = donutChart;
            }

            return await base.List(smSkip, smTake, smSortBy);
        }

        [HttpPost]
        public async Task<IActionResult> DeleteToDoItems(long id)
        {
            await using(new UnitOfWork<DataContext>())
            {
                var sisUser = await RepoFactory.Create<SISUser>().GetByIdAsync(id);
                sisUser.DeleteToDoItems();
            }
            return GoToListScreen();
        }
        #endregion
        
        #region Overrides
        protected override IActionResult StayOnDetailScreen(long id)
        {
            return GoToListScreen();
        }

        protected override IQueryable<SISUser> ApplySearchBy(IQueryable<SISUser> items, Bs4.SimpleSearchMvcModel searchBy)
        {
            items =  base.ApplySearchBy(items, searchBy);
            if (!string.IsNullOrEmpty(searchBy.SearchTerm.Value))
            {
                var searchTermLowerCase = searchBy.SearchTerm.Value.ToLower();
                items = items.Where(x => x.FirstName.ToLower().Contains(searchTermLowerCase) || 
                                         x.LastName.ToLower().Contains(searchTermLowerCase) ||
                                         x.Username.ToLower().Contains(searchTermLowerCase));
            }
            return items;
        }

        protected override IOrderedQueryable<SISUser> ApplySortBy(IQueryable<SISUser> items, string? sortBy)
        {
            if (sortBy == "ToDoItems.Count") return items.OrderBy(x => x.ToDoItems.Count);
            
            return base.ApplySortBy(items, sortBy);
        }
        #endregion
    }
}
