﻿#nullable enable

using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace WebMVC.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            return RedirectToAction("List", "ToDoItem");

            //var user = await UserHelper.GetCurrentUserAsync<SISUser, DataContext>();
            //if (user == null) throw new Exception("HomeController.Index(): user == null: this should never happen");
            //var url = Bs4.Message.RegisterSingleReadMessageAndGetUrl($"{user.FirstName} {user.LastName}, welcome to SIS. Today is {DateTime.Today.ToShortDateString()}.");
            //return LocalRedirect(url);
        }
    }
}
