﻿#nullable enable

using System.Linq;
using System.Threading.Tasks;
using Domain.Entities;
using Domain.Supermodel.Persistence;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Supermodel.Presentation.Mvc.Bootstrap4.Models;
using Supermodel.Presentation.Mvc.Controllers.Mvc;
using WebMVC.Models;

namespace WebMVC.Controllers
{
    [Authorize]
    public class DepartmentController : EnhancedCRUDController<Department, DepartmentMvcModel, Bs4.DummySearchMvcModel, DataContext> 
    { 
        #region Overrides
        protected override Task<IActionResult> AfterCreateAsync(long id, Department entityItem, DepartmentMvcModel mvcModelItem)
        {
            return Task.FromResult(StayOnDetailScreen(id));
        }
        protected override IQueryable<Department> GetItems()
        {
            return base.GetItems().Include(x => x.Majors);
        }
        #endregion
    }
}
