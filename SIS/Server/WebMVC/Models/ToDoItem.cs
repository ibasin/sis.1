﻿#nullable enable

using System.ComponentModel.DataAnnotations;
using Domain.Entities;
using Supermodel.Persistence.Entities;
using Supermodel.Presentation.Mvc.Bootstrap4.Models;

namespace WebMVC.Models
{
    public class ToDoItemMvcModel : Bs4.MvcModelForEntity<ToDoItem>
    {
        #region Overrides
        public override string Label => Name.Value;
        public override IEntity CreateEntity()
        {
            var entity = (ToDoItem)base.CreateEntity();
            entity.SISUser = new SISUser();
            return entity;
        }
        #endregion

        #region Properties
        [Required] public Bs4.TextBoxMvcModel Name { get; set; } = new Bs4.TextBoxMvcModel();
        [Required] public Bs4.TextBoxMvcModel Priority { get; set; } = new Bs4.TextBoxMvcModel();
        #endregion

    }
}
