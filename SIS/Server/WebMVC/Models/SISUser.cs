#nullable enable

using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;
using Domain.Entities;
using Microsoft.AspNetCore.Html;
using Supermodel.DataAnnotations.Validations;
using Supermodel.DataAnnotations.Validations.Attributes;
using Supermodel.Persistence.Entities;
using Supermodel.Presentation.Mvc.Bootstrap4.Models;
using Supermodel.Presentation.Mvc.Models.Api;
using Supermodel.ReflectionMapper;

namespace WebMVC.Models
{
    public class SISUserUpdatePasswordApiModel : ApiModelForEntity<SISUser>
    {
        #region Overrides
        public override Task<T> MapToCustomAsync<T>(T other)
        {
            var user = CastToEntity(other);
            user.Password = NewPassword;
            return base.MapToCustomAsync(other);
        }
        public override Task<ValidationResultList> ValidateAsync(ValidationContext validationContext)
        {
            //we don't want to the domain-level validation to be called here (which called by the base method) because we don't fill that object completely
            var vr = new ValidationResultList();
            return Task.FromResult(vr);
        }
        #endregion
        
        #region Properties
        [Required, NotRMapped] public string OldPassword { get; set; } = "";

        [Required, NotRMapped]
        public string NewPassword { get; set; } = "";
        #endregion
    }    
    
    public class SISUserUpdatePasswordMvcModel : Bs4.MvcModelForEntity<SISUser>
    {
        #region Overrides
        public override string Label => "N/A";
        public override Task<T> MapToCustomAsync<T>(T other)
        {
            var user = CastToEntity(other);
            user.Password = NewPassword.Value;
            return base.MapToCustomAsync(other);
        }
        public override Task<ValidationResultList> ValidateAsync(ValidationContext validationContext)
        {
            //we don't want to the domain-level validation to be called here (which called by the base method) because we don't fill that object completely
            var vr = new ValidationResultList();
            return Task.FromResult(vr);
        }
        #endregion

        #region Properties
        [Required, DataType(DataType.Password), NotRMapped] public Bs4.PasswordTextBoxMvcModel OldPassword { get; set; } = new Bs4.PasswordTextBoxMvcModel { PlaceholderBehavior = Bs4.PasswordTextBoxMvcModel.PlaceholderBehaviorEnum.ForceNoPlaceholder };

        [Required, DataType(DataType.Password), NotRMapped, MustEqualTo("ConfirmPassword", ErrorMessage = "Passwords do not match")]
        public Bs4.PasswordTextBoxMvcModel NewPassword { get; set; } = new Bs4.PasswordTextBoxMvcModel { PlaceholderBehavior = Bs4.PasswordTextBoxMvcModel.PlaceholderBehaviorEnum.ForceNoPlaceholder };

        [Required, DataType(DataType.Password), NotRMapped, MustEqualTo("NewPassword", ErrorMessage = "Passwords do not match")]
        public Bs4.PasswordTextBoxMvcModel ConfirmPassword { get; set; } = new Bs4.PasswordTextBoxMvcModel { PlaceholderBehavior = Bs4.PasswordTextBoxMvcModel.PlaceholderBehaviorEnum.ForceNoPlaceholder };
        #endregion
    }

    public class SISUserMvcModel: Bs4.MvcModelForEntity<SISUser>
    {
        #region Overrides
        public override NumberOfColumnsEnum NumberOfColumns => NumberOfColumnsEnum.Two;
        public override string Label => $"{FirstName.Value} {LastName.Value}";
        public override Task MapFromCustomAsync<T>(T other)
        {
            var sisUser = CastToEntity(other);
            ToDoAction = new HtmlString($@"<form method='post' action='DeleteToDoItems/{sisUser.Id}'>
                                                <button type='submit' class='btn btn-danger'>
                                                    <span class='oi oi-x'></span> 
                                                    &nbsp;
                                                    Delete all To Do Items
                                                </button>                                                
                                          </form>");

            return base.MapFromCustomAsync(other);
        }
        public override Task<T> MapToCustomAsync<T>(T other)
        {
            var user = CastToEntity(other);
            if (!string.IsNullOrEmpty(NewPassword.Value)) user.Password = NewPassword.Value;
            return base.MapToCustomAsync(other);
        }
        public override IEntity CreateEntity()
        {
            var tempEntity = (SISUser)base.CreateEntity();
            tempEntity.PasswordHashValue = "X";
            tempEntity.PasswordHashSalt = "X";
            tempEntity.SISUserPicture = new SISUserPicture();
            return tempEntity;
        }
        #endregion
        
        #region Properties
        [ListColumn(OrderBy = "Username"), Required] public Bs4.TextBoxMvcModel Username { get; set; } = new Bs4.TextBoxMvcModel();
        
        [ListColumn(OrderBy = "FirstName,LastName"), Required] public Bs4.TextBoxMvcModel FirstName { get; set; } = new Bs4.TextBoxMvcModel();
        [ListColumn(OrderBy = "LastName,FirstName"), Required] public Bs4.TextBoxMvcModel LastName { get; set; } = new Bs4.TextBoxMvcModel();
        
        [ListColumn(OrderBy ="Address.Zip")] public Bs4.USAddressMvcModel Address { get; set; } = new Bs4.USAddressMvcModel();

        [ListColumn(OrderBy = "Admin")] public Bs4.CheckboxMvcModel Admin { get; set; } = new Bs4.CheckboxMvcModel();

        [ListColumn(OrderBy = "ToDoItems.Count"), NotRMappedTo, RMapTo(".ToDoItems.Count"), ScaffoldColumn(false)] public int ToDoItemsCount { get; set; }
        
        [ListColumn, ScaffoldColumn(false), NotRMapped] public HtmlString ToDoAction { get; set; } = new HtmlString("");

        [RMapTo(".SISUserPicture.*")] public Bs4.BinaryFileMvcModel Picture { get; set; } = new Bs4.BinaryFileMvcModel();
        [ListColumn, RMapTo(".SISUserPicture.Picture"), DisplayName(" ")] public Bs4.ImageMvcModel PictureView { get; set; } = new Bs4.ImageMvcModel() { HtmlAttributesAsObj = new { height = 35} };
        
        [ForceRequiredLabel, DataType(DataType.Password), NotRMapped, MustEqualTo("ConfirmPassword", ErrorMessage = "Passwords do not match")]
        public Bs4.PasswordTextBoxMvcModel NewPassword { get; set; } = new Bs4.PasswordTextBoxMvcModel { PlaceholderBehavior = Bs4.PasswordTextBoxMvcModel.PlaceholderBehaviorEnum.ForceNoPlaceholder };

        [ForceRequiredLabel, DataType(DataType.Password), NotRMapped, MustEqualTo("NewPassword", ErrorMessage = "Passwords do not match")]
        public Bs4.PasswordTextBoxMvcModel ConfirmPassword { get; set; } = new Bs4.PasswordTextBoxMvcModel { PlaceholderBehavior = Bs4.PasswordTextBoxMvcModel.PlaceholderBehaviorEnum.ForceNoPlaceholder };
        #endregion
    }
}
