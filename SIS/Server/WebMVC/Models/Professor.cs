﻿#nullable enable

using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Domain.Entities;
using Supermodel.DataAnnotations.Validations.Attributes;
using Supermodel.Presentation.Mvc.Bootstrap4.Models;
using Supermodel.ReflectionMapper;

namespace WebMVC.Models
{
    public class Professor2MvcModel : Bs4.MvcModelForEntity<Professor>
    {
        #region Constructors
        public Professor2MvcModel()
        {
            Panels.Add(new Bs4.AccordionPanel("pt1", "Social Security", 50, 50, false));
            Panels.Add(new Bs4.AccordionPanel("pt2", "Name", 100, 100, false));
            Panels.Add(new Bs4.AccordionPanel("pt3", "Gender & Address", 200, 200, false));
        }
        #endregion
        
        #region Overrides
        public override string Label => $"{FirstName} {LastName}";
        public override bool IsDisabled => !Active;
        #endregion
        
        #region Accordion
        [NotRMapped] public List<Bs4.AccordionPanel> Panels { get; } = new List<Bs4.AccordionPanel>();
        #endregion

        #region Properties
        [ScaffoldColumn(false)] public bool Active { get; set; }
        [ScreenOrder(50), Required, HtmlAttr("marker", "M1")] public Bs4.TextBoxMvcModel SocialSecurity {get; set;} = new Bs4.TextBoxMvcModel(); 
        [Required] public Bs4.TextBoxMvcModel FirstName { get; set; } = new Bs4.TextBoxMvcModel(); 
        [Required] public Bs4.TextBoxMvcModel LastName { get; set; } = new Bs4.TextBoxMvcModel(); 
        [ScreenOrder(200)] public Bs4.DropdownMvcModelUsingEnum<GenderEnum> Gender { get; set; } = new Bs4.DropdownMvcModelUsingEnum<GenderEnum>();
        [ScreenOrder(200)] public Bs4.USAddressMvcModel HomeAddress { get; set; } = new Bs4.USAddressMvcModel();
        #endregion
    }
    
    public class ProfessorMvcModel : Bs4.MvcModelForEntity<Professor>
    {
        #region Overrides
        public override string Label => $"{FirstName} {LastName}";
        public override bool IsDisabled => !Active;
        #endregion
        
        #region Properties
        [ScaffoldColumn(false)] public bool Active { get; set; }
        [Required, HtmlAttr("marker", "M1")] public Bs4.TextBoxMvcModel SocialSecurity {get; set;} = new Bs4.TextBoxMvcModel(); 
        [Required] public Bs4.TextBoxMvcModel FirstName { get; set; } = new Bs4.TextBoxMvcModel(); 
        [Required] public Bs4.TextBoxMvcModel LastName { get; set; } = new Bs4.TextBoxMvcModel(); 
        [ScreenOrder(200)] public Bs4.DropdownMvcModelUsingEnum<GenderEnum> Gender { get; set; } = new Bs4.DropdownMvcModelUsingEnum<GenderEnum>();
        [ScreenOrder(200)] public Bs4.USAddressMvcModel HomeAddress { get; set; } = new Bs4.USAddressMvcModel();
        #endregion
    }
}
