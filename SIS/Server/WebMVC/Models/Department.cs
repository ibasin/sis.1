﻿#nullable enable

using Domain.Entities;
using Supermodel.DataAnnotations.Validations.Attributes;
using Supermodel.Presentation.Mvc.Bootstrap4.Models;
using Supermodel.Presentation.Mvc.Models;
using Supermodel.Presentation.Mvc.Models.Api;
using Supermodel.ReflectionMapper;

namespace WebMVC.Models
{
    public class DepartmentApiModel : ApiModelForEntity<Department>
    {
        #region Properties
        public string Name { get; set; } = "";

        public ListViewModel<MajorApiModel, Major> Majors { get; set; } = new ListViewModel<MajorApiModel, Major>();
        #endregion
    }
    
    public class DepartmentMvcModel : Bs4.MvcModelForEntity<Department>
    {
        #region Overrides
        public override string Label => Name.Value;
        #endregion

        #region Properties
        [ListColumn(OrderBy = "Name")] public Bs4.TextBoxMvcModel Name { get; set; } = new Bs4.TextBoxMvcModel();

        [NotRMappedTo] public ListViewModel<MajorListMvcModel, Major> Majors { get; set; } = new ListViewModel<MajorListMvcModel, Major>();
        #endregion
    }
}
