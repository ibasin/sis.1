﻿#nullable enable

using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Domain.Entities;
using Domain.Supermodel.Persistence;
using Microsoft.AspNetCore.Authorization;
using Supermodel.Presentation.Mvc.Controllers.Api;

namespace WebMVC.ApiControllers
{
    [Authorize]
    public class ProfessorLookupApiController: AutocompleteApiController<Professor, DataContext>
    {
        protected override Task<List<string>> AutocompleteAsync(IQueryable<Professor> items, string term)
        {
            return Task.FromResult(items.ToArray().Where(x => x.Active && ($"{x.FirstName} {x.LastName}").ToLower().Contains(term.ToLower())).Select(x => $"{x.FirstName} {x.LastName}").Take(25).ToList());
        }
    }
}
