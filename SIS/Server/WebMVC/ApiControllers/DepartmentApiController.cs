﻿#nullable enable

using System.Linq;
using Domain.Entities;
using Domain.Supermodel.Persistence;
using Microsoft.AspNetCore.Authorization;
using Supermodel.Presentation.Mvc.Controllers.Api;
using WebMVC.Models;

namespace WebMVC.ApiControllers
{
    [Authorize]
    public class DepartmentApiController : EnhancedCRUDApiController<Department, DepartmentApiModel, SimpleSearchApiModel, DataContext> 
    {
        protected override IQueryable<Department> ApplySearchBy(IQueryable<Department> items, SimpleSearchApiModel searchBy)
        {
            if (!string.IsNullOrEmpty(searchBy.SearchTerm)) items = items.Where(x => x.Name == searchBy.SearchTerm);

            return items;
        }
    }
}
