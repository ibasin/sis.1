﻿#nullable enable

using System.Threading.Tasks;
using Domain.Entities;
using Domain.Supermodel.Persistence;
using Microsoft.AspNetCore.Authorization;
using Supermodel.Persistence.Repository;
using Supermodel.Persistence.UnitOfWork;
using Supermodel.Presentation.Mvc.Controllers.Api;

namespace WebMVC.ApiControllers
{
    [Authorize]
    public class ActivateProfessorsApiController 
        : CommandApiController<ActivateProfessorsApiController.Input, ActivateProfessorsApiController.Output>
    {
        #region Embedded Types
        public class Input
        {
            #region Properties
            public string Suffix {get; set; } = "";
            #endregion
        }

        public class Output
        {
            #region Properties
            public int ReactivatedCount { get; set; }
            #endregion
        }
        #endregion

        #region Overrides
        protected override async Task<Output> ExecuteAsync(Input input)
        {
            var output = new Output();

            await using (new UnitOfWork<DataContext>())
            {
                var professors = await RepoFactory.Create<Professor>().GetAllAsync();
                foreach (var professor in professors)
                {
                    if (!professor.Active)
                    {
                        professor.Active = true;
                        professor.LastName += input.Suffix;
                        output.ReactivatedCount++;
                    }
                }
            }

            return output;
        }
        #endregion
    }
}
