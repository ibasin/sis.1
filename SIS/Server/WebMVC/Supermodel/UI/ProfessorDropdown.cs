﻿#nullable enable

using System;
using Microsoft.AspNetCore.Html;
using Microsoft.AspNetCore.Mvc.Rendering;
using Supermodel.Presentation.Mvc.Bootstrap4.Models;
using Supermodel.Presentation.Mvc.Extensions;
using WebMVC.Models;

namespace WebMVC.Supermodel.UI
{
    public class ProfessorDropdown : Bs4.DropdownMvcModelUsing<ProfessorMvcModel>
    {
        #region Overrides
        public override IHtmlContent DisplayTemplate<T>(IHtmlHelper<T> html, int screenOrderFrom = Int32.MinValue,
            int screenOrderTo = Int32.MaxValue, string? markerAttribute = null)
        {
            var baseDisplay = base.DisplayTemplate(html, screenOrderFrom, screenOrderTo, markerAttribute);
            return (baseDisplay.GetString() + DisabledSuffix).ToHtmlEncodedHtmlString();
        }
        #endregion
    }
}
