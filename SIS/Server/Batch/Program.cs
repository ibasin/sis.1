#nullable enable

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Domain.Entities;
using Domain.Supermodel.Persistence;
using Microsoft.EntityFrameworkCore;
using Supermodel.Persistence.EFCore;
using Supermodel.Persistence.Repository;
using Supermodel.Persistence.UnitOfWork;
using Supermodel.ReflectionMapper;

namespace Batch
{
    class Program
    {
        static async Task Main()
        {
            Console.WriteLine("Press Enter to continue...");
            Console.ReadLine();

            //Comment this out if you don't want to recreate and re-seed db every time you start the app in debug mode
            if (Debugger.IsAttached)
            {
                Console.Write("Recreating the database... ");
                await using (new UnitOfWork<DataContext>())
                {
                    await EFCoreUnitOfWorkContext.Database.EnsureDeletedAsync();
                    await EFCoreUnitOfWorkContext.Database.EnsureCreatedAsync();
                    await UnitOfWorkContext.SeedDataAsync();
                }
                Console.WriteLine("Done!");
            }

            await using(new UnitOfWork<DataContext>())
            {
                var majors = await RepoFactory.Create<Major>().GetAllAsync();
                
                var whatley = new Student { FirstName = "Tim", LastName = "Whatley", Gender = GenderEnum.Male, SocialSecurity = "555-55-5555", Major = majors.Single(x => x.Name == "Math")};
                whatley.Add();

                var newman = new Student { FirstName = "Norman", LastName = "Newman", Gender = GenderEnum.Male, SocialSecurity = "666-66-6666", Major = majors.Single(x => x.Name == "English") };
                newman.Add();

                var seinfield = await LinqRepoFactory.Create<Student>().Items.SingleAsync(x => x.FirstName == "Jerry" && x.LastName == "Seinfeld");
                seinfield.SocialSecurity = "000-00-0000";

                var costanza = await LinqRepoFactory.Create<Student>().Items.SingleAsync(x => x.FirstName == "George" && x.LastName == "Costanza");
                costanza.Delete();

                //UnitOfWorkContext.CommitOnDispose = false;
            }

            //await using(new UnitOfWork<DataContext>())
            //{
            //    var mathMajor = await LinqRepoFactory.Create<Major>().Items.SingleAsync(x => x.Name == "Math");
            //    mathMajor.Delete();
            //}

            //await using (new UnitOfWork<DataContext>())
            //{
            //    var repo = LinqRepoFactory.Create<Student>();
            //    var students = await repo.Items.Where(x => x.Gender == GenderEnum.Male).ToListAsync();

            //    foreach (var student in students)
            //    {
            //        student.Delete();
            //    }
            //}

            await using (new UnitOfWork<DataContext>(ReadOnly.Yes))
            {
                var repo = RepoFactory.Create<Student>();
                var students = await repo.GetAllAsync();
                PrintStudents(students);
            }
            
            Console.WriteLine();

            await using (new UnitOfWork<DataContext>(ReadOnly.Yes))
            {
                var repo = LinqRepoFactory.Create<Student>();
                var students = await repo.Items.Where(x => x.Gender == GenderEnum.Male).ToListAsync();
                //var repo = (StudentRepo)RepoFactory.Create<Student>();
                //var students = await repo.GetByGenderAsync(GenderEnum.Male);
                PrintStudents(students);
            }
        }

        private static void PrintStudents(List<Student> students)
        {
            foreach (var student in students)
            {
                Console.WriteLine($"{student.FirstName} {student.LastName}, {student.Gender.GetDescription()}, { student.SocialSecurity }, { student.Major.Name }");
            }
        }
    }
}
